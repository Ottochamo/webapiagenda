
USE agendain6av2;

DELIMITER &&
CREATE TRIGGER tr_historialContactoInsert	
	AFTER INSERT    
	ON DetalleUsuario FOR EACH ROW
    BEGIN
    INSERT INTO Historial(idUsuario, mensaje) 
		VALUES(NEW.idUsuario, 'Contacto creado');
    END&&
DELIMITER ;
    
DELIMITER &&
CREATE TRIGGER tr_historialContactoUpdate
	BEFORE UPDATE ON Contacto
    FOR EACH ROW
    BEGIN
    
    DECLARE _idUsuario INT;
    
	SET _idUsuario = (SELECT idUsuario FROM detalleusuario
		WHERE idContacto = new.idContacto);
    
		INSERT INTO Historial(idUsuario, mensaje)
			VALUES(_idUsuario, 'Contacto actualizado');
    END&&
DELIMITER ;

DELIMITER &&
CREATE TRIGGER tr_historialContactoDelete
	BEFORE DELETE ON DetalleUsuario
	FOR EACH ROW
    BEGIN
    INSERT INTO Historial(idUsuario,  mensaje, fecha)
		VALUES(OLD.idUsuario, 'Contacto Eliminado');
    
    END&&
DELIMITER ;

DROP TRIGGER tr_historialContactoDelete

DELIMITER &&
CREATE TRIGGER tr_editarUsuario
	AFTER UPDATE ON Usuario
    FOR EACH ROW
    BEGIN
		INSERT INTO Historial(idUsuario, mensaje)
			VALUES(NEW.idUsuario, 'Edito su cuenta');
    END&&

DELIMITER ;

DELIMITER &&

CREATE TRIGGER tr_historialCitaInsert
	AFTER INSERT ON DetalleCita
    FOR EACH ROW
	BEGIN 
		INSERT INTO Historial(idUsuario, mensaje) 
			VALUES(NEW.idUsuario, 'Cita creada');
    END&&
    
DELIMITER ;

DELIMITER &&
	CREATE TRIGGER tr_historialCitaUpdate
		AFTER UPDATE ON DetalleCita
			FOR EACH ROW
            BEGIN
				INSERT INTO Historial(idUsuario, mensaje)
					VALUES(NEW.idUsuario, 'Cita editada');
            END&&
DELIMITER ;

DELIMITER &&
	CREATE TRIGGER tr_historialCitaDelete
		AFTER DELETE ON DetalleCita
		FOR EACH ROW
        BEGIN
			INSERT INTO Historial(idUsuario, mensaje)
				VALUES(OLD.idUsuario, 'Cita eliminada');
        END&&
DELIMITER ;

DELIMITER &&

CREATE TRIGGER tr_historialTareaInsert
	AFTER INSERT ON DetalleTarea
    FOR EACH ROW
	BEGIN 
		INSERT INTO Historial(idUsuario, mensaje) 
			VALUES(NEW.idUsuario, 'Tarea creada');
    END&&
    
DELIMITER &&

DELIMITER &&
	CREATE TRIGGER tr_historialTareaUpdate
		AFTER UPDATE ON DetalleTarea
			FOR EACH ROW
            BEGIN
				INSERT INTO Historial(idUsuario, mensaje)
					VALUES(NEW.idUsuario, 'Tarea editada');
            END&&
DELIMITER ;

DELIMITER &&
	CREATE TRIGGER tr_historialTareaDelete
		AFTER DELETE ON DetalleTarea
		FOR EACH ROW
        BEGIN
			INSERT INTO Historial(idUsuario, mensaje)
				VALUES(OLD.idUsuario, 'Tarea eliminada');
        END&&
DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_SelectHistorial(IN _idUsuario INT)
		BEGIN
			SELECT * FROM historial_Usuario
				WHERE idUsuario = _idUsuario;
        END&&
DELIMITER ;


