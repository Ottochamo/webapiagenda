
DELIMITER &&
	CREATE PROCEDURE sp_insertarCita(_idContacto INT, _idUsuario INT, _fecha DATETIME,
		_lugar VARCHAR(100), _descripcion VARCHAR(100))
        BEGIN
		DECLARE _idCita INT;
        
			INSERT INTO Cita(idContacto, fecha, lugar, descripcion)
				VALUES(_idContacto, _fecha, _lugar, _descripcion);
			
            SET _idCita = (SELECT MAX(idCita) FROM Cita);
                
			INSERT INTO DetalleCita(idCita, idUsuario) VALUES(_idCita, _idUsuario);
                
        END&&
DELIMITER ; 


DELIMITER &&
	CREATE PROCEDURE sp_editarCita(_idContacto INT, _idCita INT, _fecha DATETIME,
		_lugar VARCHAR(100), _descripcion VARCHAR(100))
        BEGIN
			UPDATE Cita SET idContacto = _idContacto, fecha = _fecha,
				lugar = _lugar, descripcion = _descripcion WHERE idCita = _idCita;
        END&&
DELIMITER ; 

DELIMITER &&
	CREATE PROCEDURE sp_eliminarCita(_idCita INT)
        BEGIN
			DELETE FROM Cita WHERE idCita = _idCita;
        END&&
DELIMITER ; 

DELIMITER &&
	CREATE PROCEDURE sp_selectCita(_idCita INT)
        BEGIN
			SELECT * FROM Cita WHERE idCita = _idCita;
        END&&
DELIMITER ; 

DELIMITER &&
	CREATE PROCEDURE sp_selectCitas(_idUsuario INT)
        BEGIN
			SELECT * FROM cita_usuario WHERE idUsuario = _idUsuario;
        END&&
DELIMITER ; 
