USE Agendain6av2;

DELIMITER &&

CREATE PROCEDURE sp_InsertarContacto (IN _idUsuario INT, IN _idCategoria INT , IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(100), _foto VARCHAR(100))
	BEGIN
    DECLARE _idContacto INT;
    
    INSERT INTO Contacto(idCategoria, nombre, apellido, 
		direccion, telefono, correo, foto)         
        VALUES(_idCategoria, _nombre, _apellido, _direccion, _telefono, _correo, _foto);
        
    
    SET _idContacto = (SELECT MAX(idContacto) FROM Contacto LIMIT 1);
    
    INSERT INTO DetalleUsuario(idUsuario, idContacto)
		VALUES(_idUsuario, _idContacto);
    
    END&&
    
DELIMITER ;   

CALL sp_insertarContacto(1, 1, 'otto', 'chamo', 'casa', '30001673', 'ottoc904@gmail.com', '')

DELIMITER $$

CREATE PROCEDURE sp_EditarContacto(IN _idContacto INT, IN _idCategoria INT,  
	IN _nombre VARCHAR(100), IN _apellido VARCHAR(100), 
    IN _direccion VARCHAR(100), IN _telefono VARCHAR(100), IN _correo VARCHAR(100), _foto VARCHAR(100))
	BEGIN
    
    UPDATE Contacto SET idCategoria = _idCategoria, nombre = _nombre, apellido = _apellido,
    direccion = _direccion, telefono = _telefono, correo = _correo, foto = _foto
    WHERE idContacto = _idContacto;
    
    END$$
    
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE sp_EliminarContacto (IN _idContacto INT)
	BEGIN
    DELETE FROM Contacto WHERE idContacto = _idContacto;
    END$$

DELIMITER ;


DELIMITER &&
	CREATE PROCEDURE sp_contactoUsuario(_idUsuario INT)
		BEGIN
			SELECT * FROM contacto_usuario WHERE idUsuario = _idUsuario;
        END&&
DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_Contacto(_idContacto INT)
		BEGIN
			SELECT * FROM contacto WHERE idContacto = _idContacto;
        END&&
DELIMITER ;
