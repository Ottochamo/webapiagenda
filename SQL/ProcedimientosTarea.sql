USE agendain6av2;

DELIMITER &&
	CREATE PROCEDURE sp_InsertarTarea(IN _idPrioridad INT, IN _nombre VARCHAR(100), IN _descripcion VARCHAR(100),
		IN _fecha DATETIME, IN _idUsuario INT)
    BEGIN
		DECLARE _idTarea INT;
		
        INSERT INTO Tarea(idPrioridad, nombre, descripcion, fecha) VALUES(_idPrioridad, _nombre, _descripcion, _fecha);
        
        SET _idTarea = (SELECT MAX(idTarea) FROM Tarea);
		INSERT INTO DetalleTarea(idTarea, idUsuario) 
			VALUES(_idTarea, _idUsuario);
    END&&
DELIMITER ;


DELIMITER &&
	CREATE PROCEDURE sp_EditarTarea(IN _idTarea INT, IN _idPrioridad INT, 
		IN _nombre VARCHAR(100), IN _descripcion VARCHAR(100), IN _fecha DATETIME)
	BEGIN
		UPDATE Tarea SET idTarea = _idTarea, idPrioridad = _idPrioridad, nombre = _nombre, descripcion = _descripcion, 
        fecha = _fecha WHERE idTarea = _idTarea;
    END&&
DELIMITER ;


DELIMITER &&
	CREATE PROCEDURE sp_EliminarTarea(IN _idTarea INT)
    BEGIN
		DELETE FROM Tarea WHERE idTarea = _idTarea;
    END&&

DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_SelectTareas(IN _idUsuario INT)
		BEGIN
			SELECT * FROM tarea_usuario WHERE
				idUsuario = _idUsuario;
        END&&
DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_SelectTarea(IN _idTarea INT)
		BEGIN
			SELECT idTarea, idPrioridad, nombre, descripcion, date_format(fecha, '%d-%m-%') AS fecha FROM Tarea WHERE
				idTarea = _idTarea;
        END&&
DELIMITER ;


INSERT INTO Prioridad(descripcion) VALUES('Baja');
INSERT INTO Prioridad(descripcion) VALUES('Media');
INSERT INTO Prioridad(descripcion) VALUES('Alta');INSERT INTO Prioridad(descripcion) VALUES('Baja')
