USE agendain6av2;


CREATE VIEW
 contacto_usuario
	AS
    SELECT c.idContacto, c.nombre, c.apellido, c.direccion, c.telefono, u.idUsuario, 
    c.correo, c.idCategoria, category.nombreCategoria
    FROM Contacto AS c
    INNER JOIN Categoria AS category ON c.idCategoria = category.idCategoria
    INNER JOIN DetalleUsuario AS det ON det.idContacto = c.idContacto
    INNER JOIN Usuario AS u ON u.idUsuario = det.idUsuario
    ORDER BY c.idContacto;
    

CREATE VIEW 
	categoria_usuario
	AS
	SELECT c.idCategoria, c.nombreCategoria AS nombre, u.idUsuario
	FROM Categoria AS c
    INNER JOIN DetalleCategoria AS det ON det.idCategoria = c.idCategoria
    INNER JOIN Usuario AS u ON det.idUsuario = u.idUsuario;
    
    
CREATE VIEW
	cita_usuario
	AS
    SELECT det.idCita, c.idContacto, con.nombre, u.idUsuario, DATE_FORMAT(c.fecha, '%d-%m-%Y') AS fecha, c.lugar, c.descripcion FROM
    Cita AS c
    INNER JOIN Contacto AS con ON c.idContacto = con.idContacto
    INNER JOIN DetalleCita AS det ON c.idCita = det.idCita
    INNER JOIN Usuario AS u ON u.idUsuario = det.idUsuario;
    
CREATE VIEW
	tarea_usuario
    AS
    SELECT t.idTarea, p.idPrioridad, p.descripcion AS prioridad,
    t.nombre, t.descripcion, date_format(fecha, '%d-%m-%Y') AS fecha, u.idUsuario
    FROM Tarea AS t
    INNER JOIN Prioridad AS p ON P.idPrioridad = t.idPrioridad
    INNER JOIN DetalleTarea AS det ON det.idTarea = t.idTarea
    INNER JOIN Usuario AS u ON det.idUsuario = u.idUsuario;
    


CREATE VIEW historial_usuario
	AS 
    SELECT h.idHistorial, h.idUsuario, h.mensaje, 
		DATE_FORMAT(fecha, '%d/%m/%Y') AS fecha, 
			DATE_FORMAT(fecha, '%h-%m-%s') AS hora FROM
	Historial AS h;
    
