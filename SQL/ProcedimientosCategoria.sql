USE agendain6av2;

DELIMITER &&
	CREATE PROCEDURE sp_InsertarCategoria(IN _idUsuario INT, IN _nombreCategoria VARCHAR(100))
		BEGIN
		DECLARE _idCategoria INT;
        
        INSERT INTO Categoria (nombreCategoria) VALUES(_nombreCategoria);
        
        SET _idCategoria = (SELECT MAX(idCategoria) FROM Categoria);
        
			INSERT INTO DetalleCategoria(idUsuario, idCategoria) 
				VALUES(_idUsuario, _idCategoria);
        END&&
DELIMITER ;

CALL sp_InsertarCategoria(1, 'Amigos');
CALL sp_InsertarCategoria(1, 'Familia');
CALL sp_InsertarCategoria(1, 'Kinal');


DELIMITER &&
	CREATE PROCEDURE sp_UpdateCategoria(IN _idCategoria INT,
		_nombreCategoria VARCHAR(100))
        BEGIN
			UPDATE Categoria SET nombreCategoria = _nombreCategoria
				WHERE idCategoria = _idCategoria;
        END&&
DELIMITER;

DELIMITER &&
	CREATE PROCEDURE sp_DeleteCategoria(IN _idCategoria INT)
		BEGIN
			DELETE FROM Categoria WHERE idCategoria = _idCategoria;
        END&&
DELIMITER ;

DELIMITER &&
CREATE PROCEDURE sp_getCatUsuario(IN _idUsuario INT)
	BEGIN
		SELECT * FROM categoria_usuario WHERE idUsuario = _idUsuario;
    END&&
DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_selectCategorias(IN _idCategoria INT)
    BEGIN
		SELECT * FROM Categoria WHERE idCategoria = _idCategoria;
    END&&
DELIMITER ;
