CREATE DATABASE AgendaIn6av2;

USE AgendaIn6av2;

CREATE TABLE Categoria (
	idCategoria INT AUTO_INCREMENT  NOT NULL,
    nombreCategoria VARCHAR(100) NOT NULL,
    PRIMARY KEY (idCategoria)
);

CREATE TABLE Contacto (
	idContacto INT AUTO_INCREMENT  NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100) NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    telefono VARCHAR(100) NOT NULL,
    correo VARCHAR(100) NOT NULL,
    idCategoria INT NOT NULL,
    PRIMARY KEY(idContacto),
    FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
    ON DELETE CASCADE
);
CREATE TABLE Usuario (
	idUsuario INT AUTO_INCREMENT  NOT NULL,
    nick VARCHAR(100) NOT NULL,
    contrasena VARCHAR(100) NOT NULL,
    PRIMARY KEY (idUsuario)
);


ALTER TABLE Usuario ADD COLUMN foto VARCHAR(100) DEFAULT 'http://localhost:3000/images/perfil2.jpg' NULL;
ALTER TABLE Contacto ADD COLUMN foto VARCHAR(100) DEFAULT 'http://localhost:3000/images/perfil2.jpg' NULL;




CREATE TABLE DetalleCategoria(
	idDetalleCategoria INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idCategoria INT NOT NULL,
	CONSTRAINT pk_DetalleCategoria PRIMARY KEY (idDetalleCategoria),
    CONSTRAINT fk_DetalleCatUsuario FOREIGN KEY (idUsuario) 
		REFERENCES Usuario(idUsuario)
        ON DELETE CASCADE,
    CONSTRAINT fk_DetalleCatidCategoria FOREIGN KEY (idCategoria) 
		REFERENCES Categoria(idCategoria)
	ON DELETE CASCADE
);


CREATE TABLE Cita (
	idCita INT AUTO_INCREMENT NOT NULL,
	idContacto INT NOT NULL,
    fecha DATETIME NOT NULL,
    lugar VARCHAR(100) NOT NULL,
    descripcion VARCHAR(100)  NOT NULL,
    CONSTRAINT pk_Cita PRIMARY KEY (idCita),
    CONSTRAINT fk_idContacto FOREIGN KEY (idContacto) 
    REFERENCES Contacto(idContacto)
    ON DELETE CASCADE
);

CREATE TABLE DetalleCita (
	idDetalleCita INT AUTO_INCREMENT  NOT NULL,
	idCita INT NOT NULL,
    idUsuario INT NOT NULL,
    CONSTRAINT pk_DetalleCita PRIMARY KEY (idDetalleCita),
    CONSTRAINT fk_idCita FOREIGN KEY (idCita) REFERENCES Cita(idCita)
    ON DELETE CASCADE,
    CONSTRAINT fk_idDetalleCitaUsuario FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
    ON DELETE CASCADE
);

CREATE TABLE DetalleUsuario (
	idDetalleUsuario INT AUTO_INCREMENT NOT NULL,
    idUsuario INT NOT NULL,
    idContacto INT NOT NULL,
    CONSTRAINT pk_idDetalleUsuario PRIMARY KEY (idDetalleUsuario),
    CONSTRAINT fk_detalleUsuarioidUsuario FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    CONSTRAINT fk_detalleUsuarioidContacto FOREIGN KEY (idContacto) REFERENCES Contacto(idContacto)
    ON DELETE CASCADE
);

-- DROP TABLE DetalleUsuario;

CREATE TABLE Prioridad (
	idPrioridad INT AUTO_INCREMENT NOT NULL ,
    descripcion VARCHAR(100) NOT NULL,
    CONSTRAINT pk_idPrioridad PRIMARY KEY(idPrioridad)
);

CREATE TABLE Tarea (
	idTarea INT AUTO_INCREMENT NOT NULL ,
    idPrioridad INT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    descripcion VARCHAR(100) NOT NULL,
    fecha DATETIME NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_idTarea PRIMARY KEY (idTarea),
    CONSTRAINT fk_idPrioridad FOREIGN KEY (idPrioridad) 
		REFERENCES Prioridad(idPrioridad)
	ON DELETE CASCADE
);

CREATE TABLE DetalleTarea (
	idDetalleTarea INT AUTO_INCREMENT NOT NULL,
    idUsuario INT NOT NULL,
    idTarea INT NOT NULL,
    CONSTRAINT pk_idDetalleTarea PRIMARY KEY(idDetalleTarea),
    CONSTRAINT fk_idUsuario FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
    ON DELETE CASCADE,
    CONSTRAINT fk_idTarea FOREIGN KEY(idTarea) REFERENCES Tarea(idTarea)
    ON DELETE CASCADE

);

CREATE TABLE TiemposTarea (
	idTiempo INT AUTO_INCREMENT NOT NULL,
    idTarea INT NOT NULL,
    tiempo DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY(idTiempo),
    FOREIGN KEY (idTarea) REFERENCES Tarea(idTarea)
    ON DELETE CASCADE
);

CREATE TABLE Historial (
	idHistorial INT AUTO_INCREMENT NOT NULL,
    idUsuario INT NOT NULL,
	mensaje VARCHAR(100) NOT NULL,
    fecha DATETIME DEFAULT NOW() NOT NULL,
    PRIMARY KEY (idHistorial),
    CONSTRAINT fk_idUsuarioHistorial 
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
    ON DELETE CASCADE
);
