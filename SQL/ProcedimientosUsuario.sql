USE AgendaIn6av2;

DELIMITER &&
CREATE PROCEDURE sp_Autenticar(IN _nick VARCHAR(100), IN _contrasena VARCHAR(100))
	BEGIN
		SELECT * FROM Usuario WHERE
        nick = _nick AND contrasena = _contrasena;
    END&&
DELIMITER ;

DELIMITER &&
CREATE PROCEDURE sp_selectUsuario(IN _idUsuario INT)
	BEGIN
		SELECT * FROM Usuario
			WHERE idUsuario = _idUsuario;
    END&&
DELIMITER ;

DELIMITER &&

CREATE PROCEDURE sp_InsertUsuario(IN _nick VARCHAR(100), IN _contrasena VARCHAR(100), _foto VARCHAR(100))
	BEGIN
    INSERT INTO Usuario(nick, contrasena, foto) VALUES(_nick, _contrasena, _foto);
    END&&

DELIMITER ;

CALL sp_InsertUsuario('otto', 'otto', '');


DELIMITER &&
CREATE PROCEDURE sp_UpdateUsuario(IN _idUsuario INT, IN _nick VARCHAR(100), 
	IN _contrasena VARCHAR(100), _foto VARCHAR(100))
	BEGIN
		UPDATE Usuario set nick = _nick, contrasena = _contrasena, foto = _foto
			WHERE idUsuario = _idUsuario;
    END&&
DELIMITER ; 


DELIMITER &&
CREATE PROCEDURE sp_EliminarUsuario(IN _idUsuario INT)
	BEGIN
    DELETE FROM Usuario WHERE idUsuario = _idUsuario;
    
    END&&
    
DELIMITER ;


