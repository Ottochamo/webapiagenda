var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors');


//IMPORTAR ROUTES
var indexRoute = require('./routes/index');
var authRoute = require('./routes/auth.route');
var usuarioRoute = require('./routes/api/usuario.route');
var categoriaRoute = require('./routes/api/categoria.route');
var contactoRoute = require('./routes/api/contacto.route');
var tareaRoute = require('./routes/api/tarea.route');
var prioridadRoute = require('./routes/api/prioridad.route');
var uploadRoute = require('./routes/api/upload.route');
var citaRoute = require('./routes/api/cita.route');
var historiaRoute = require('./routes/api/historial.route');
var app = express();
var port = 3000;
var uri = '/api/v1/';

//CONFIGURACION VISTA
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//CONFIGURACION LOGGER
app.use(logger('dev'));

//CONFIGURACION DE BODY-PARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Accept, Authorization');

	if ('OPTIONS' == req.method) {
		res.sendStatus(200);
	} else {
		next();
	}
});

app.use('/', indexRoute);
app.use('/', authRoute);
app.use(uri, usuarioRoute);
app.use(uri, categoriaRoute);
app.use(uri, contactoRoute);
app.use(uri, tareaRoute);
app.use(uri, prioridadRoute);
app.use(uri, citaRoute);
app.use(uri, historiaRoute);
app.use('/', uploadRoute);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');

  next();
});


app.listen(port, function() {
  console.log("El servidor esta corriendo puerto: " + port);
});
