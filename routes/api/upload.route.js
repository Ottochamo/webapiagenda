
var formidable = require('formidable');
var fs = require('fs-extra')
var express = require('express');
var services = require('../../services');
var router = express.Router();

router.post('/uploadUser', services.verificar, function(req, res, next) {
    let form = formidable.IncomingForm();
    let filePath = 'http://localhost:3000/';

    form.parse(req)

    form.on('fileBegin', function(name, file) {      
      file.path = './public/images/'+ file.name;
    });

    form.on('end', function() {
      let oldPath = this.openedFiles[0].path;
      let newPath = './public/images/' + req.usuario.idUsuario + '/' + this.openedFiles[0].name;
      filePath += '/images/' + req.usuario.idUsuario + '/' + this.openedFiles[0].name;
      fs.copy(oldPath, newPath, function(error) {
        if (error) {
          console.log(error);
          res.sendStatus(500);
        } else {
          res.json({path: filePath});
        }
      })
    })

});


module.exports = router;
