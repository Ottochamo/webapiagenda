var express = require('express');
var services = require('../../services');
var tarea = require('../../model/tarea.model');
var router = express.Router();

router.get('/tarea', services.verificar, function(req, res) {
    let idUsuario = req.usuario.idUsuario;

    tarea.selectAll(idUsuario, function(resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
           res.json({'mensaje': 'no hay tareas'});
        }
    })
})

router.get('/tarea/:idTarea', services.verificar, function(req, res) {
    let idTarea = req.params.idTarea;

    tarea.selectOne(idTarea, function(resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados[0]);            
        } else {
            res.json({'mensaje': 'no hay tareas'});
        }
    })

}); 

router.post('/tarea', services.verificar, function(req, res) {
    let data = {
        idUsuario: req.usuario.idUsuario,
        idPrioridad: req.body.idPrioridad,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        fecha: req.body.fecha
    };

    tarea.insert(data, function(resultados) {
        if (resultados && resultados.affectedRows > 0) {
            res.json({'mensaje': 'guardado'});
        } else {
            res.json({'mensaje': 'no se guardo'});
        }
    })

});

router.put('/tarea/:idTarea', services.verificar, function(req, res) {
    let data = {
        idUsuario: req.usuario.idUsuario,
        idTarea: req.body.idTarea,
        idPrioridad: req.body.idPrioridad,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        fecha: req.body.fecha
    };

    if (data.idTarea == req.params.idTarea) {
        tarea.update(data, function(resultado) {
            if (resultado.affectedRows > 0) {
                res.json({'mensaje': 'actualizado'});
            } else {
                res.json({'mensaje': 'no se pudo actualizar'});
            }
        }) 
    } else {
        res.json({'mensaje': 'no coinciden datos'});
    }

});

router.delete('/tarea/:idTarea', services.verificar, function(req, res) {
    let idTarea = req.params.idTarea;

    tarea.delete(idTarea, function(resultado) {
        if (resultado) {
            res.json(resultado);
        } else {
            res.json({'mensaje': 'error no se pudo eliminar'});
        }
    })

});


module.exports = router;