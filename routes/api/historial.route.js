var express = require('express');

var historial = require('../../model/historial.model');
var services = require('../../services');

var router = express.Router();

router.get('/historial', services.verificar, function(req, res) {
    historial.selectAll(req.usuario.idUsuario, function(resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'mensaje': 'no hay prioridades'});
        }
    })
});

module.exports = router;