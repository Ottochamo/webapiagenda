var express = require('express');
var cita = require('../../model/cita.model');
var services = require('../../services');

var router = express.Router();

router.get('/cita', services.verificar, function(req, res) {
    let idUsuario = req.usuario.idUsuario;

    cita.selectAll(idUsuario, function(resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
           res.json({'mensaje': 'no hay tareas'});
        }
    })
})

router.get('/cita/:idTarea', services.verificar, function(req, res) {
    let idTarea = req.params.idTarea;

    cita.selectOne(idTarea, function(resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados[0]);            
        } else {
            res.json({'mensaje': 'no hay tareas'});
        }
    })

}); 

router.post('/cita', services.verificar, function(req, res) {
    console.log(JSON.stringify(req.body));
    let data = {
        idUsuario: req.usuario.idUsuario,
        idContacto: req.body.idContacto,
        lugar: req.body.lugar,
        descripcion: req.body.descripcion,
        fecha: req.body.fecha
    };

    console.log(data);

    cita.insert(data, function(resultados) {
        if (resultados && resultados.affectedRows > 0) {
            res.json({'mensaje': 'guardado'});
        } else {
            res.json({'mensaje': 'no se guardo'});
        }
    })

});

router.put('/cita/:idCita', services.verificar, function(req, res) {
    let data = {
        idUsuario: req.usuario.idUsuario,
        idCita: req.body.idCita,
        idContacto: req.body.idContacto,
        lugar: req.body.lugar,
        descripcion: req.body.descripcion,
        fecha: req.body.fecha
    };

    if (data.idCita == req.params.idCita) {
        cita.update(data, function(resultado) {
            if (resultado.affectedRows > 0) {
                res.json({'mensaje': 'actualizado'});
            } else {
                res.json({'mensaje': 'no se pudo actualizar'});
            }
        }) 
    } else {
        res.json({'mensaje': 'no coinciden datos'});
    }

});

router.delete('/cita/:idCita', services.verificar, function(req, res) {
    let idCita = req.params.idCita;

    cita.delete(idCita, function(resultado) {
        if (resultado) {
            res.json(resultado);
        } else {
            res.json({'mensaje': 'error no se pudo eliminar'});
        }
    })

});


module.exports = router;
