var express = require('express');

var prioridad = require('../../model/prioridad.model');

var router = express.Router();

router.get('/prioridad', function(req, res) {
    prioridad.selectAll(function(resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'mensaje': 'no hay prioridades'});
        }
    })
});

module.exports = router;