var express = require('express');
var router = express.Router();
var categoria = require('../../model/categoria.model');

router.get('/categorias/:idUsuario', function(req, res, next) {
  let idUsuario = req.params.idUsuario;
  categoria.select(idUsuario, function(categorias) {
    if(typeof categorias !== 'undefined') {
      res.json(categorias);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});

router.get('/categoria/:idCategoria', function(req, res, next) {
  let idCategoria = req.params.idCategoria;

  categoria.selectOne(idCategoria, function(error, results) {
    if (typeof results !== 'undefined') {
      res.json(results[0]);
    } else {
      res.json({'mensaje': 'no hay categorias'});
    }
  })

})

router.post('/categoria', function(req, res, next) {
  var data = {
    idCategoria : null,
    idUsuario : req.body.idUsuario,
    nombreCategoria : req.body.nombreCategoria
  }

  categoria.insert(data, function(resultado){
    console.log(JSON.stringify(resultado));
    if(resultado !== undefined) {
      res.json({'mensaje': 'ingresado con exito'});
    } else {
      res.json({"mensaje":"No se ingreso la categoria"});
    }
  });
});


router.put('/categoria/:idCategoria', function(req, res, next){

  var data = {
    idCategoria : req.params.idCategoria,
    nombreCategoria : req.body.nombreCategoria
  }

  categoria.update(data, function(resultado){

    if(typeof resultado !== 'undefined') {
      res.json(resultado);
    } else {
      res.json({"mensaje":"No se pudo actualizar"});
    }

  });
});

router.delete('/categoria/:idCategoria', function(req, res, next){
  var idCategoria = req.params.idCategoria;

  categoria.delete(idCategoria, function(resultado){
    if(resultado && resultado.mensaje === "Eliminado") {
      res.json({"mensaje":"Se elimino la categoria correctamente"});
    } else {
      res.json({"mensaje":"Se elimino la categoria"});
    }
  });
});

module.exports = router;
