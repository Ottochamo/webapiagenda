var express = require('express');
var contacto = require('../../model/contacto.model');
var services = require('../../services');
var router = express.Router();

router.get('/contacto/', services.verificar, function(req, res, next) {
  contacto.select(req.usuario.idUsuario, function(contactos) {
    if(typeof contactos !== 'undefined') {
      res.json(contactos[0]);
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});

router.get('/contacto/:id', services.verificar,function(req, res) {
  let idContacto = req.params.id;

  contacto.selectOne(idContacto, function(error, results) {
    console.log(JSON.stringify(results));
    if (typeof results !== 'undefined') {
      res.json(results);
    } else {
      res.json({'Mensaje': 'No hay contactos'});
    }
  })
})

router.post('/contacto', services.verificar,function(req, res, next) {
  var data = {
    idUsuario: req.body.idUsuario,
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    direccion : req.body.direccion,
    correo : req.body.correo,
    foto: (req.body.foto == undefined) ? '' : req.body.foto,
    idCategoria : req.body.idCategoria
  };
  console.log(req.body);
  console.log('DATA: '+JSON.stringify(data));

  contacto.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({'json': 'exito'});
    } else {
      res.json({"mensaje":"No se ingreso el contacto"});
    }
  });
});

router.put('/contacto/:idContacto', services.verificar,function(req, res, next){
  var idContacto = req.params.idContacto;
  var data = {
    idContacto : req.body.idContacto,
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    direccion : req.body.direccion,
    correo : req.body.correo,
    idCategoria : req.body.idCategoria,
    foto: (req.body.foto == undefined) ? null : req.body.foto
  }
  if(idContacto == data.idContacto) {
    contacto.update(data, function(resultado){
      if(resultado.affectedRows > 0) {
        console.log(JSON.stringify(resultado));
        res.json(resultado);
      } else {
        console.log("NO: " + resultado);
        //res.json({"estatus": "false"});
        res.end();
      }
    });
  } else {
    res.json({"mensaje": "No coinciden los identificadores"});
  }
});

router.delete('/contacto/:idContacto', services.verificar, function(req, res, next){
  var idContacto = req.params.idContacto;

  contacto.delete(idContactoUri, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
        res.json({"mensaje":"Se elimino el contacto correctamente"});
    } else {
        res.json({"mensaje":"Se elimino el contacto"});
    }
  });
});

module.exports = router;
