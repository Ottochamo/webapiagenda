var database = require("../config/database.config");
var Categoria = {};

Categoria.select = function(idUsuario, callback) {
  if(database) {
		database.query('CALL sp_getCatUsuario(?)', idUsuario, function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

Categoria.selectOne = function(idCategoria, callback) {
  if (database) {
    database.query('CALL sp_selectCategorias(?)', idCategoria, function(error, results) {
      if (error) {
        throw error;
      } else {
        callback(null, results[0]);
      }
    });
  }
}

Categoria.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_InsertarCategoria(?,?)', [data.idUsuario, data.nombreCategoria],
      function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"insertId":resultado.insertId});
      }
    });
  }
}

Categoria.update = function(data, callback){
	if(database) {
		database.query('UPDATE Categoria SET nombreCategoria=? WHERE idCategoria=?',
		[data.nombreCategoria, data.idCategoria],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(data);
			}
		});
	}
}

Categoria.delete = function(idCategoria, callback) {
	if(database) {
		database.query('DELETE FROM Categoria WHERE idCategoria = ?', idCategoria,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Categoria;
