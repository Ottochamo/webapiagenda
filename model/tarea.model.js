var database = require('../config/database.config');

var tarea = {};

tarea.selectAll = function(idUsuario, callback) {
    if (database) {
        database.query('CALL sp_SelectTareas(?)' , idUsuario, function(error, resultados) {
            if (error) {
                throw error;
            } else {
                callback(resultados[0]);
            }
        })
    }
}

tarea.selectOne = function(idTarea, callback) {
    if (database) {
        database.query('CALL sp_SelectTarea(?)', idTarea, function(error, resultados) {
            if (error) {
                throw error; 
            } else {
                callback(resultados[0]);
            }
        })
    }
}

tarea.insert = function(data, callback) {
    if (database) {
        database.query('CALL sp_InsertarTarea(?, ?, ?, ?, ?)', [data.idPrioridad, 
            data.nombre, data.descripcion, data.fecha, data.idUsuario], function(error, resultados) {
            if (error) {
                throw error;
            } else {
                callback({"affectedRows": resultados.affectedRows});
            }
        });
    }
}

tarea.update = function(data, callback) {
    if (database) {
        database.query('CALL sp_EditarTarea(?, ?, ?, ?, ?)', [data.idTarea, data.idPrioridad, 
            data.nombre, data.descripcion, data.fecha], function(error, resultado) {
                if (error) {
                    throw error;
                } else {
                    callback({'affectedRows': resultado.affectedRows});
                }
            })
    }
}

tarea.delete = function(idTarea, callback) {
    if (database) {
        database.query('CALL sp_EliminarTarea(?)', idTarea, function(error, resultados) {
            if (error) {
                throw error; 
            } else {
                callback({'mensaje': 'elimando con exito'});
            }
        })
    }
}

module.exports = tarea;
