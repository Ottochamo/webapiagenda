var database = require("../config/database.config");
var Contacto = {};

Contacto.select = function(idUsuario, callback) {
  if(database) {
		database.query('CALL sp_ContactoUsuario(?)', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Contacto.selectOne = function(idContacto, callback) {
  if (database) {
    database.query('CALL sp_Contacto(?)', idContacto,
      function(error, resultado) {
        if (error) {
          throw error;
        } else {
          callback(null, resultado[0]);
        }
      });
    }
}

Contacto.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_insertarContacto(?,?,?,?,?,?,?,?)',
    [data.idUsuario, data.idCategoria, data.nombre, data.apellido, data.direccion,
      data.telefono, data.correo, data.foto],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Contacto.update = function(data, callback){
	if(database) {
		console.log(JSON.stringify(data))
		database.query('CALL sp_editarContacto(?,?,?,?,?,?,?,?)',
		[data.idContacto, data.idCategoria, data.nombre,
			data.apellido, data.direccion, data.telefono, data.correo, data.foto],
			
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado);
			}
		});
	}
}

Contacto.delete = function(idContacto, callback) {
	if(database) {
		database.query('CALL sp_deleteContacto(?)', idContacto,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Contacto;
