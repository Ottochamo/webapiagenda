var database = require('../config/database.config');

var historial = {};

historial.selectAll = function(idUsuario, callback) {
    if (database) {
        database.query('CALL sp_SelectHistorial(?)', idUsuario, function(error,
                 resultados) {
            if (error) {
                throw error;
            } else {
                callback(resultados[0]);
            }
        });
    }
}

module.exports = historial;
