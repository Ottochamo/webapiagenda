var database = require('../config/database.config');

var prioridad = {};

prioridad.selectAll = function(callback) {
    if (database) {
        database.query('SELECT * FROM Prioridad', function(error,
                 resultados) {
            if (error) {
                throw error;
            } else {
                callback(resultados)
            }
        });
    }
}

module.exports = prioridad;
