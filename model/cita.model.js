var express = require('express');
var database = require('../config/database.config');
var cita = {};

cita.selectAll = function(idUsuario, callback) {
    if (database) {
        database.query('CALL sp_selectCitas(?)' , idUsuario, function(error, resultados) {
            if (error) {
                throw error;
            } else {
                console.log(resultados[0])
                callback(resultados[0]);
            }
        })
    }
}

cita.selectOne = function(idCita, callback) {
    if (database) {
        database.query('CALL sp_selectCita(?)', idCita, function(error, resultados) {
            if (error) {
                throw error; 
            } else {
                callback(resultados[0]);
            }
        })
    }
}

cita.insert = function(data, callback) {
    if (database) {
        database.query('CALL sp_InsertarCita(?, ?, ?, ?, ?)', [data.idContacto, 
            data.idUsuario, data.fecha, data.descripcion, data.lugar], function(error, resultados) {
            if (error) {
                throw error;
            } else {
                callback({"affectedRows": resultados.affectedRows});
            }
        });
    }
}

cita.update = function(data, callback) {
    if (database) {
        database.query('CALL sp_EditarCita(?, ?, ?, ?, ?)', [data.idContacto, data.idCita, 
            data.fecha, data.lugar, data.descripcion], function(error, resultado) {
                if (error) {
                    throw error;
                } else {
                    callback({'affectedRows': resultado.affectedRows});
                }
            })
    }
}

cita.delete = function(idCita, callback) {
    if (database) {
        database.query('CALL sp_EliminarCita(?)', idCita, function(error, resultados) {
            if (error) {
                throw error; 
            } else {
                callback({'mensaje': 'elimando con exito'});
            }
        })
    }
}

module.exports = cita;